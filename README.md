# String embedding in Ada specs

## What is this?

This is a small Ruby script that reads one or more files and creates an Ada package spec file (.ads with the GNAT name convention) that contains the files as `String` variables.

## Why?

Sometimes (not often, but it happens) a program of mine needed data contained in a text file that the user did not need to know about; in a sense the file could be considered as part of the whole program, not just an external data source.  In this case it makes sense to have the file content "embedded" inside the application, without the need of an external resource (that is bothersome to carry about, it can go missing, it can get corrupted, ...)

A solution is to define inside the Ada code a string with the file content; but if the file is large, you can imagine that creating such Ada string by hand is quite bothersome.  Therefore, I wrote this small script to help me in this type of conversion.  It started as a small thing and now it is a nice and useful (to my opinion, but you know, [*Ogne scarrafone è bell' a mamma soja*](https://dettieproverbi.it/proverbi/campania/ogne-scarrafone-e-bell-a-mamma-soja/)) piece of software.   I used it, for example, in the project [eugen](https://gitlab.com/mockturtle/eugen)

If you think it can be useful for you too, please help yourself...


## How do I install it?

Just put [emebed_in_ads.rb](emebed_in_ads.rb) where your shell will find it and set it executable.  That's it.

## How do you use it?

### Fast and dirty description

Suppose, for example, that you want to create file `foo-bar-zoo.ads` (that contains the specs of Foo.Bar.Zoo) from files `gin.txt` and `tonic.txt` you can use

```sh
          embed_in_ads.rb --output=foo-bar-zoo.ads gin.txt tonic.txt
```

The content of `gin.txt` will be assigned to constant `Gin : constant String`, and the content of `tonic.txt` will be assigned to `Tonic : constant String`, both in package `Foo.Bar.Zoo`  If you want to use, say, `Vodka`  instead of `Gin` you can use

```sh
          embed_in_ads.rb --output=foo-bar-zoo.ads vodka=gin.txt tonic.txt
```

If you want the package private you can use `-p` or `--private` as in 
```sh
          embed_in_ads.rb --private --output=foo-bar-zoo.ads vodka=gin.txt tonic.txt
```

### Detailed description

The command syntax is as follows

```bash
       embed_in_ads.rb [options] [input_specs ...]
```
that is, any number of  options followed by any number of `input_specs`

* An **input_spec** has the format `[varname=]filename` where
  * `filename` is the file to embed
  * `varname` is the name of the corresponding variable inside the package.  If `varname` is missing, it is obtained from `filename` according the following algorithm
    * Any extension is removed from `filename`
    * Every character that is not legal in an Ada identifier (i.e., non-alphanumeric characters that are not `_`) are replaced with undrescores `_`
    * Multiple consecutive underscores are coalesced in one
    * Trailing and leading underscores are removed
  * If `filename` is `-` the standard input is used
  * The default `varname` for the standard input is "Value"
  * If no `input_spec` is present, the standard input is read and `varname` is "Value"
* The **options** are
  * **-p** or **--private** Create a private package
  * **-PNAME** or **--package=NAME** specify the package name
  * **-oFILENAME** or **--output=FILENAME** specify the output filename

Depending on the presence of the package name or the output name, the following defaults are used

|                                |  package name given  |    package name **not** given      |
| ------------------------------ | -------------------- | ---------------------------------- |
| output filename given          | Value given used     | package name derived from filename |
| output filename **not** given  | standard output used |           Error                    |

>>>
One could wonder why use the standard output when only the package is specified instead of obtaining the filename from the package name.  This is because it is not possible to derive the target directory from the package name.  True, maybe the current directory was a sensible default, but this choice allows to use the program as a filter, e.g.,
```bash
   data_producer | embed_in_ads.rb --package=foo.bar.zoo  | gnatpp > /tmp/foo-bar-zoo.ads
```
or, alternatively,
```bash
   data_producer | embed_in_ads.rb --package=foo.bar.zoo data=-  | gnatpp > /tmp/foo-bar-zoo.ads
```
to use variable `Data` instead of the default `Value`
>>>
