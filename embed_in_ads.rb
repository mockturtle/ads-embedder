#!/usr/bin/env ruby
require 'optparse'

def die(msg="")
  $stderr.puts "\nError: #{msg}" if msg.size > 0

  $stderr.puts <<EOF

Usage: #{$0} target_package_file input_spec [input_spec ...] 

* target_package_file is supposed to follow the GNAT convention
* input_spec has the format 
 
              ID=filename

ID is a valid Ada identifier that will be used as variable name 
in the generated Ada file.  As a shortcut, if only one input_spec 
is present, it can be of the form

             filename

that is, with no ID, and as ID will be used "Value"

EOF
  exit 1
end

def default_extension(filename, ext)
  if File.extname(filename) != ext
    return filename + ext
  else
    return filename
  end
end

class GNAT_handler
  #
  # This script uses "name handlers" to map filenames to package names
  # and handling, if required, default extensions.  A name handler must
  # provide a function normalize to handle default extensions and
  # a function to_package_name that maps a filename to a package
  #
  def normalize(filename)
    default_extension(filename, '.ads')
  end
  
  def to_package_name(filename)
    parts = File.basename(filename, '.ads').split('-')

    return parts.map {|x| x.capitalize}.join('.')
  end
end

class Tabbed_Stream
  #
  # A tabbed stream is similar to a stream and provides functions
  # print, puts and newline; but it can add tabbing.
  #
  def initialize(stream)
    @stream = stream
    @tabbing = []
    @begin_line = true
  end

  def tab(size=4)
    #
    # Add size spaces to the current tabbing.  If a block is given,
    # it is called and the tabbing popped at the end of the block
    # execution
    #
    @tabbing.push(size)
    
    if block_given?
      yield
      self.untab
    end
  end

  def untab
    @tabbing.pop 
  end

  def puts(s="")
    add_spacer_if_needed

    @stream.puts(s)
    @begin_line = true
  end

  def print(s="")
    if s != ""
      add_spacer_if_needed
      
      @stream.print(s)
      @begin_line = false
    end
  end

  def newline
    @stream.puts("")
  end

  private
  
  def add_spacer_if_needed
    if @begin_line 
      t = @tabbing.inject(0) {|accum, value| accum+value}
      @stream.print(' '*t)
      @begin_line=false
    end
  end
end

def adaize_char(c)
  case c
  when "\n"
    return "Latin_1.LF"

  when "\r"
    return "Latin_1.CR"

  when "\t"
    return "Latin_1.HT"

  else
    return "Character'Val(#{c.ord})"
  end
end

def print_adaized_string(item, stream)
  lines = []
  accum = ''

  item.each_char do |c|
    if c >= ' ' && c <= '~'
      accum += c
      accum += '"' if c == '"'
    else
      if accum.size > 0
        quoted = '"' + accum + '"'        
        accum=''
        
        if c=="\n"
          lines << quoted + ' & ' + adaize_char(c)
          c=nil
        else
          lines << quoted
        end
      end

      lines << adaize_char(c) unless c.nil?
    end
  end

  lines << accum if accum.size > 0

  (0...lines.size).each do |idx|
    head = (idx==0) ? "  " : "& ";
    stream.print(head + lines[idx])

    if idx < lines.size-1
      stream.puts
    end
  end
end

def remove_trailing_leading_underscores(input)
  #
  # Aren't regexps nice? :-) Yes, I know, it looks like noise...
  #
  # The part extracted is the section within the external pair
  # of parenthesis.  The expression inside said pair matches
  #
  # * A single character that is not an underscore, OR
  #
  # * A multi-character string that begins and ends with characters that
  #   are not an underscore
  #
  # The extracted text can be preceded or followed by any number of
  # underscores
  #
  input.match(/^_*([^_](.*[^_])?)_*$/)

  die("Bad name: '#{input}'") if $1.nil? # This should never happen, anyway...
  
  return $1
end

def filename_to_ada_id(filename)
  no_extension = File.basename(filename, '.*')
  parts = no_extension.split(/[^A-Za-z0-9_]/).delete_if{|part| part.empty?}

  return remove_trailing_leading_underscores(parts.join('_'))
end

Input_Spec = Struct.new("Input_Spec", "var_name", "filename");

def parse_input_specs
  used = Hash.new
  
  result = Array.new
  
  while ARGV.size > 0
    var, filename=ARGV.shift.split('=',2)
    
    if filename.nil?
      filename=var
      var = filename_to_ada_id(filename)
    end

    die("Variable name #{var} used twice") if used[var]

    used[var]=true
    result << Input_Spec.new(var, filename)
  end

  if result.empty?
    result << Input_Spec.new("Value", "-");
  end
  
  return result
end

def slurp(input)
  if input == '-'
    $stdin.read
  else
    File.read(input)
  end
end

def de_extensionize(input)
  idx = input.rindex('.')
  result = idx.nil? ? input : input[0...idx]
end

def print_multiline(lines, tabbed, var_name)
  lines.map! {|x| x.chomp}
  max_length = lines.map {|x| x.size}.max
  
  tabbed.puts "subtype #{var_name}_type is String(1..#{max_length});"
  tabbed.puts "#{var_name} : constant array(1..#{lines.size}) of #{var_name}_type :="

  first_entry=true
  
  tabbed.tab do
    tabbed.puts "("

    lines.each do |line|      
      padding = max_length - line.size
      padded = line + (' ' * padding)

      if !first_entry then
        tabbed.puts ","
      end

      first_entry=false

      tabbed.print "\"#{padded}\"" 
    end

    tabbed.puts ");";
  end
end


private_package = false
package_name    = nil
output          = nil
name_convention = "gnat"
automagic       = false
multiline       = false

OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [options]"

  opts.on("-p", "--private", "Make the package private") do
    die("Private option given twice") if private_package
    private_package = true
  end

  opts.on("-c", "--compiler", "Make #{$0} act as a 'compiler'") do
    die("Automagic option given twice") if automagic
    automagic = true
  end

  opts.on("-PNAME", "--package=NAME", "package name") do |name|
    die("Package name given twice") unless package_name.nil?
    package_name = name
  end

  opts.on("-oFILENAME", "--output=FILENAME", "Output filename") do |filename|
    die("Output filename given twice") unless output.nil?
    output=filename
  end

  opts.on("-M", "--multi-line", "Output an arrat with a string per line") do
    multiline=true
  end
end.parse!

input_specs = parse_input_specs

if automagic
  die("Both output and automagic specified") unless output.nil?

  die("Too many inputs with automagic") unless input_specs.size == 1

  input = input_specs[0].filename

  output = de_extensionize(input).downcase + '.ads'

  if File.exists?(output)
    if File.mtime(output) >= File.mtime(input)
      exit 0
    end
  end
end

if output.nil?
  die("Neither package name nor output file")  if package_name.nil?

  output_stream=$stdout
else
  name_handler  = GNAT_handler.new
  output_stream = File.open(name_handler.normalize(output), 'w')
  package_name  = name_handler.to_package_name(output) if package_name.nil?
end

tabbed = Tabbed_Stream.new(output_stream)

tabbed.puts <<EOF
with Ada.Characters.Latin_1;  use Ada.Characters;
--
-- WARNING: This files was automatically generated by files
--   #{input_specs.map {|x| x.filename}.join(', ')}
--
-- any manual change will be lost.
--
package #{package_name} is

EOF


input_specs.each do |spec|
  tabbed.tab do

    if multiline then
      print_multiline(File.readlines(spec.filename), tabbed, spec.var_name);
    else
      tabbed.puts "#{spec.var_name} : constant String :="

      tabbed.tab do
        print_adaized_string(slurp(spec.filename), tabbed);
        tabbed.puts ";"
      end
    end

    tabbed.newline
  end
end

tabbed.puts "end #{package_name};"



